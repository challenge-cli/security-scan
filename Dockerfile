FROM ubuntu:18.04

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

RUN apt-get update -y \
    && apt-get install -y  wget=1.19.* apt-transport-https gnupg lsb-release \
    && wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | tee /usr/share/keyrings/trivy.gpg > /dev/null \
    && echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | tee -a /etc/apt/sources.list.d/trivy.list \
    && apt-get update -y \
    && apt-get install -y --no-install-recommends trivy=0.41.0 \    
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/bin/trivy"]